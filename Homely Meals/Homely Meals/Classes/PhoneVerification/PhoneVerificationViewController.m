//
//  PhoneVerificationViewController.m
//  Homely Meals
//
//  Created by Ratheesh V.R on 11/01/15.
//  Copyright (c) 2015 Outplay Labs. All rights reserved.
//

#import "PhoneVerificationViewController.h"
#import "AppController.h"
#import "AppState.h"
#import "Reachability.h"
#import "APIRequest.h"

@interface PhoneVerificationViewController ()
@property(nonatomic,strong) APIRequest* apirequest;
@end

@implementation PhoneVerificationViewController

-(IBAction)verifySelected:(id)sender{
    [self.view endEditing:YES];
    if(![self getNetworkStatus]){
        UIAlertView* alertview = [[UIAlertView alloc] initWithTitle:@"No Internet" message:@"Please connect to internet" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alertview show];
        return;
    }

    
    NSString* tmpCountryCode = [self.textfieldVerificationCode.text stringByReplacingOccurrencesOfString:@" " withString:@""];
    if([tmpCountryCode isEqualToString:@""]){
        UIAlertView* alertview = [[UIAlertView alloc] initWithTitle:@"Invalid Verification Code" message:@"Please enter proper verification code" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alertview show];
        return;
    }
    
    [[AppController sharedAppController] showOverlayInView:self.view];
    AppState* appstate = [AppState sharedAppState];
    
    
    NSDictionary* param = [[NSDictionary alloc] initWithObjectsAndKeys:
                           appstate.phonenumber, @"phone",
                           tmpCountryCode,@"verification_code",
                           nil];
    
    //NSString* urlstr = @"http://172.16.9.192:8000/homelymealsuser/verify/";
    NSString* urlstr = @"http://homelymeals.herokuapp.com/homelymealsuser/verify/";
    NSDictionary* data = [[NSDictionary alloc] initWithObjectsAndKeys:
                          urlstr,@"URL",
                          param,@"Param"
                          , nil];
    
    if(!self.apirequest)
        self.apirequest = [[APIRequest alloc] init];
    [self.apirequest postWithData:data ofType:1 delegate:self];
}

- (void) apiCallDidFinish:(NSData*) result header:(NSDictionary*) headerinfo ofType:(NSInteger) type{
    NSDictionary* data = (NSDictionary*)result;

    [[AppController sharedAppController] removeOverlay];
    NSInteger status = [[data valueForKey:@"status"] integerValue];
    if(status == 1){
        [[AppController sharedAppController] showMealsList];
        [[AppState sharedAppState] updateVerified:YES];
    }
    else{
        UIAlertView* alertview = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Invalid verififcation code." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alertview show];
    }

}
- (void) apiCallDidFail:(NSString*) result ofType:(NSInteger) type{
    
    [[AppController sharedAppController] removeOverlay];
    UIAlertView* alertview = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Unable to connect. Please try after some time." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
    [alertview show];
    
}

-(BOOL) getNetworkStatus{
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    if( [reachability currentReachabilityStatus] == NotReachable)
        return NO;
    
    return YES;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self setSingleTapGesture];
}

#pragma mark Single Tap
-(void) setSingleTapGesture{
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onSingleClickAction:)];
    [tapGesture setNumberOfTapsRequired:1];
    [tapGesture setNumberOfTouchesRequired:1];
    [self.view addGestureRecognizer:tapGesture];
}
- (void) onSingleClickAction:(UITapGestureRecognizer *) recognizer {
    [self.view endEditing:YES];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
