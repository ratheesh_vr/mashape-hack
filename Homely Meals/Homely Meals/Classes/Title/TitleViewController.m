//
//  TitleViewController.m
//  Homely Meals
//
//  Created by Ratheesh V.R on 11/01/15.
//  Copyright (c) 2015 Outplay Labs. All rights reserved.
//

#import "TitleViewController.h"
#import "AppController.h"
#import "AppState.h"

@interface TitleViewController ()

@end

@implementation TitleViewController

-(void) showNextScreen{
    AppState* appstate = [AppState sharedAppState];
    if(appstate.isVerified)
       [[AppController sharedAppController] showMealsList];
    else
        [[AppController sharedAppController] showPhoneEntry];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self performSelector:@selector(showNextScreen) withObject:self afterDelay:1.0f];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
