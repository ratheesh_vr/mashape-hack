//
//  AppController.h
//  Homely Meals
//
//  Created by Ratheesh V.R on 11/01/15.
//  Copyright (c) 2015 Outplay Labs. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AppController : NSObject
@property(nonatomic) NSInteger currentScreen;

-(void) showOverlayInView:(UIView*) view;
-(void) removeOverlay;

-(void) showPhoneEntry;
-(void) showPhoneVerification;
-(void) showMealsList;
-(void) showNewMeals;
-(void) showTitle;
+(id) sharedAppController;
@end
