//
//  APIRequest.m
//  iCrushiFlush
//
//  Created by Ratheesh V.R on 30/07/14.
//  Copyright (c) 2014 Outplay Labs. All rights reserved.
//

#import "APIRequest.h"
#import "AFNetworking.h"

#import "JSONKit.h"
#import "AppState.h"

#define INTERNET_TIMEOUT 60.0f

@interface APIRequest(){
    NSInteger requestType;
}
@end

@implementation APIRequest

-(void) postWithData:(NSDictionary*) data ofType:(NSInteger) requesttype delegate:(id) del{
    
    self.delegate = del;
    requestType = requesttype;
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSDictionary *parameters = [data valueForKey:@"Param"];
    
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    NSLog(@"parameters: %@",parameters);
NSLog(@"parameters: %@",[data valueForKey:@"URL"]);
    
    [manager POST:[data valueForKey:@"URL"] parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {

        //NSLog(@"*************");
        //NSLog(@"Response: %@",responseObject );
        

        
        [self.delegate apiCallDidFinish:responseObject header:nil ofType:requestType];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        //NSLog(@"Error: %@", error);
        //[[AppState sharedAppState] checkIsServerDown];
        [self.delegate apiCallDidFail:[error localizedDescription] ofType:requestType];
    }];
    
}
-(void) getWithData:(NSDictionary*) data ofType:(NSInteger) requesttype delegate:(id) del{
    
    self.delegate = del;
    requestType = requesttype;
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSDictionary *parameters = [data valueForKey:@"Param"];
    
    NSLog(@"parameters: %@",parameters);

    [manager GET:[data valueForKey:@"URL"] parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        

        [self.delegate apiCallDidFinish:responseObject header:nil ofType:requestType];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);

        [self.delegate apiCallDidFail:[error localizedDescription] ofType:requestType];
    }];
    
}

@end
