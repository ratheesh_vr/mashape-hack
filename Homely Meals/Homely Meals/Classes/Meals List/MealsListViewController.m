//
//  MealsListViewController.m
//  Homely Meals
//
//  Created by Ratheesh V.R on 11/01/15.
//  Copyright (c) 2015 Outplay Labs. All rights reserved.
//

#import "MealsListViewController.h"
#import "AppController.h"
#import "MealsTableViewCell.h"
#import "Reachability.h"
#import "APIRequest.h"
#import "AppState.h"
#import "LocationManager.h"

@interface MealsListViewController ()
@property(nonatomic,strong) NSMutableArray* arrayMeals;
@property(nonatomic,strong) APIRequest* apirequest;
@end

@implementation MealsListViewController


-(IBAction)refreshSelected:(id)sender{
    if(![self getNetworkStatus]){
        UIAlertView* alertview = [[UIAlertView alloc] initWithTitle:@"No Internet" message:@"Please connect to internet" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alertview show];
        return;
    }
    [self.arrayMeals removeAllObjects];
    [[AppController sharedAppController] showOverlayInView:self.view];
    LocationManager* locationmanager = [LocationManager sharedLocationManager];
    AppState* appstate = [AppState sharedAppState];
    NSString* urlstr = [NSString stringWithFormat:@"http://homelymeals.herokuapp.com/foodentry/get/?latitude=%f&longitude=%f&phone=%@",locationmanager.latitude,locationmanager.longitude,appstate.phonenumber];
    NSDictionary* param = [[NSDictionary alloc] init];
    NSDictionary* data = [[NSDictionary alloc] initWithObjectsAndKeys:
                          urlstr,@"URL",
                          param,@"Param"
                          , nil];

    [self.apirequest getWithData:data ofType:1 delegate:self];
}


- (void) apiCallDidFinish:(NSData*) result header:(NSDictionary*) headerinfo ofType:(NSInteger) type{
    NSDictionary* data = (NSDictionary*)result;
    [[AppController sharedAppController] removeOverlay];
    [self.arrayMeals removeAllObjects];
    [self.arrayMeals addObjectsFromArray:[data valueForKey:@"entry_details"]];
    
    [self.tableviewMeals reloadData];
   // NSLog(@"LOgin Data: %@",data);
    //NSLog(@"self.arrayMeals: %@",self.arrayMeals);
}
- (void) apiCallDidFail:(NSString*) result ofType:(NSInteger) type{
    NSLog(@"LOgin fail: %@",result);
    [[AppController sharedAppController] removeOverlay];
    UIAlertView* alertview = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Unable to connect. Please try after some time." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
    [alertview show];
    
}


-(BOOL) getNetworkStatus{
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    if( [reachability currentReachabilityStatus] == NotReachable)
        return NO;
    
    return YES;
}
#pragma mark - UITableViewController


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
{

    return [self.arrayMeals count];

}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 143;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    static NSString *CellIdentifier = @"MealsCell";
    
    MealsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil)
    {
        NSArray* views = [[NSBundle mainBundle] loadNibNamed:@"MealsTableViewCell" owner:nil options:nil];
        
        for (UIView *view in views) {
            if([view isKindOfClass:[UITableViewCell class]])
            {
                cell = (MealsTableViewCell*)view;

                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                
            }
        }
    }

    
    NSDictionary* data = (NSDictionary*) [self.arrayMeals objectAtIndex:indexPath.row];
    cell.labelMealName.text = [data valueForKey:@"entry_name"];
    cell.labelPrice.text = [NSString stringWithFormat:@"%@",[data valueForKey:@"price"]]; ;
    cell.labelDistance.text = [NSString stringWithFormat:@"%@",[data valueForKey:@"distance"]];
    cell.labelAvialable.text = [data valueForKey:@"available_upto"];
    cell.labelQantity.text = [NSString stringWithFormat:@"%@",[data valueForKey:@"entry_quantity"]]; ;
    cell.labelContact.text =  [NSString stringWithFormat:@"%@",[data valueForKey:@"phone"]]; ;
    return cell;
}

-(IBAction)newmealsSelected:(id)sender{
    [[AppController sharedAppController] showNewMeals];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.arrayMeals = [[NSMutableArray alloc] init];
    
    self.apirequest = [[APIRequest alloc] init];
    [self refreshSelected:nil];
    
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
