//
//  MealsListViewController.h
//  Homely Meals
//
//  Created by Ratheesh V.R on 11/01/15.
//  Copyright (c) 2015 Outplay Labs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MealsListViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>
@property(nonatomic,weak) IBOutlet UITableView* tableviewMeals;
-(IBAction)newmealsSelected:(id)sender;
-(IBAction)refreshSelected:(id)sender;
@end
