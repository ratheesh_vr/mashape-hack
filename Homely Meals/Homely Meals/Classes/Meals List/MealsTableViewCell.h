//
//  MealsTableViewCell.h
//  Homely Meals
//
//  Created by Ratheesh V.R on 11/01/15.
//  Copyright (c) 2015 Outplay Labs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MealsTableViewCell : UITableViewCell
@property(nonatomic,weak) IBOutlet UILabel* labelMealName;
@property(nonatomic,weak) IBOutlet UILabel* labelPrice;
@property(nonatomic,weak) IBOutlet UILabel* labelDistance;
@property(nonatomic,weak)  IBOutlet UILabel* labelAvialable;
@property(nonatomic,weak) IBOutlet UILabel* labelQantity;
@property(nonatomic,weak) IBOutlet UILabel* labelContact;
@end
