//
//  Constants.h
//  Homely Meals
//
//  Created by Ratheesh V.R on 11/01/15.
//  Copyright (c) 2015 Outplay Labs. All rights reserved.
//
enum {
    kScreenTitle = 1,
    kScreenPhoneNumberEntry,
    kScreenPhoneNumberVerification,
    kScreenMealsList,
    kScreenNewMeals,
    
};