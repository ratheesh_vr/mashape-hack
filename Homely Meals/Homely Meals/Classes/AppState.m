//
//  AppState.m
//  Homely Meals
//
//  Created by Ratheesh V.R on 11/01/15.
//  Copyright (c) 2015 Outplay Labs. All rights reserved.
//

#import "AppState.h"

@implementation AppState

-(void) updateVerified:(BOOL) vf{
    NSUserDefaults* userdeaults = [NSUserDefaults standardUserDefaults];
    self.isVerified = vf;
    [userdeaults setBool:self.isVerified forKey:@"IS_VERIFIED"];
    [userdeaults synchronize];
}
-(void) updatePhoneNumber:(NSString*) ph{
    self.phonenumber = ph;
    NSUserDefaults* userdeaults = [NSUserDefaults standardUserDefaults];
    [userdeaults setValue:self.phonenumber forKeyPath:@"PHONE_NUMBER"];
    [userdeaults synchronize];
}
-(void) loadState{
    NSUserDefaults* userdeaults = [NSUserDefaults standardUserDefaults];
    NSInteger saved = [userdeaults integerForKey:@"SAVED"];
    if(saved == 1){
        self.isVerified = [userdeaults boolForKey:@"IS_VERIFIED" ];
        self.phonenumber = [userdeaults stringForKey:@"PHONE_NUMBER" ];
    }
    else{
        [userdeaults setInteger:1 forKey:@"SAVED"];
        self.isVerified = NO;
        self.phonenumber = @"";
        [userdeaults setBool:self.isVerified forKey:@"IS_VERIFIED"];
        [userdeaults setValue:@"" forKeyPath:@"PHONE_NUMBER"];
        [userdeaults synchronize];

    }
    
}
-(id) init{
    self = [super init];
    if(self){
        [self loadState];
    }
    return self;
}

+(id) sharedAppState{
    static AppState *sharedAppState = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedAppState = [[self alloc] init];
    });
    return sharedAppState;
}
@end
