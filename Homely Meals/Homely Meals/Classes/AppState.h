//
//  AppState.h
//  Homely Meals
//
//  Created by Ratheesh V.R on 11/01/15.
//  Copyright (c) 2015 Outplay Labs. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AppState : NSObject
-(void) updateVerified:(BOOL) vf;
-(void) updatePhoneNumber:(NSString*) ph;
@property(nonatomic) BOOL isVerified;
@property(nonatomic,strong) NSString* phonenumber;

+(id) sharedAppState;
@end
