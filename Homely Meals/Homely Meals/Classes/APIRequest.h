//
//  APIRequest.h
//  iCrushiFlush
//
//  Created by Ratheesh V.R on 30/07/14.
//  Copyright (c) 2014 Outplay Labs. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol APIRequestDelegate
- (void) apiCallDidFinish:(NSData*) result header:(NSDictionary*) headerinfo ofType:(NSInteger) type;
- (void) apiCallDidFail:(NSString*) result ofType:(NSInteger) type;
@end

@interface APIRequest : NSObject
@property (nonatomic, weak) id<APIRequestDelegate> delegate;
-(void) postWithData:(NSDictionary*) data ofType:(NSInteger) requesttype delegate:(id) del;
-(void) getWithData:(NSDictionary*) data ofType:(NSInteger) requesttype delegate:(id) del;

@end
