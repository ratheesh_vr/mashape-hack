//
//  AppController.m
//  Homely Meals
//
//  Created by Ratheesh V.R on 11/01/15.
//  Copyright (c) 2015 Outplay Labs. All rights reserved.
//

#import "AppController.h"
#import "AppDelegate.h"
#import "RootViewController.h"
#import "Constants.h"
#import "TitleViewController.h"
#import "PhoneNumberEntryViewController.h"
#import "PhoneVerificationViewController.h"
#import "MealsListViewController.h"
#import "NewMealsViewController.h"
#import "OverlayView.h"

@interface AppController ()
@property(nonatomic,strong) TitleViewController* titleController;
@property(nonatomic,strong) PhoneNumberEntryViewController* phoneentryController;
@property(nonatomic,strong) PhoneVerificationViewController* phoneverificationController;
@property(nonatomic,strong) MealsListViewController* mealslistController;
@property(nonatomic,strong) NewMealsViewController* newmealsController;
@property(nonatomic,strong) OverlayView* overlayView;

@end


@implementation AppController

-(void) showOverlayInView:(UIView*) view{
    if(!self.overlayView){
        self.overlayView = [[OverlayView alloc] initWithFrame:CGRectMake(0, 0, 320, 568)];
    }
    self.overlayView.frame = CGRectMake(0, 0, 320, 568);
    [view addSubview:self.overlayView];
    [self.overlayView.activityIndicator startAnimating];
    
}
-(void) removeOverlay{
    [self.overlayView.activityIndicator stopAnimating];
    [self.overlayView removeFromSuperview];
}

-(void) showPhoneEntry{
    if(self.currentScreen == kScreenPhoneNumberEntry)
        return;
    
    if(!self.phoneentryController){
        self.phoneentryController = [[PhoneNumberEntryViewController alloc] initWithNibName:@"PhoneNumberEntryViewController" bundle:nil];
    }
    [self removeCurretnView];
    [self addToAppView:self.phoneentryController.view];
    self.currentScreen = kScreenPhoneNumberEntry;
}
-(void) showPhoneVerification{
    if(self.currentScreen == kScreenPhoneNumberVerification)
        return;
    
    if(!self.phoneverificationController){
        self.phoneverificationController = [[PhoneVerificationViewController alloc] initWithNibName:@"PhoneVerificationViewController" bundle:nil];
    }
    [self removeCurretnView];
    [self addToAppView:self.phoneverificationController.view];
    self.currentScreen = kScreenPhoneNumberVerification;
}
-(void) showMealsList{
    
    if(self.currentScreen == kScreenMealsList)
        return;
    
    if(!self.mealslistController){
        self.mealslistController = [[MealsListViewController alloc] initWithNibName:@"MealsListViewController" bundle:nil];
    }
    [self removeCurretnView];
    [self addToAppView:self.mealslistController.view];
    self.currentScreen = kScreenMealsList;
    
}
-(void) showNewMeals{
    if(self.currentScreen == kScreenNewMeals)
        return;
    
    if(!self.newmealsController){
        self.newmealsController = [[NewMealsViewController alloc] initWithNibName:@"NewMealsViewController" bundle:nil];
    }
    [self removeCurretnView];
    [self addToAppView:self.newmealsController.view];
    self.currentScreen = kScreenNewMeals;
}

-(void) showTitle{
    if(self.currentScreen == kScreenTitle)
        return;
    
    if(!self.titleController){
        self.titleController = [[TitleViewController alloc] initWithNibName:@"TitleViewController" bundle:nil];
    }
    [self addToAppView:self.titleController.view];
    self.currentScreen = kScreenTitle;
    
    
}

-(void) removeCurretnView{
    switch (self.currentScreen) {
        case kScreenTitle:
            [self.titleController.view removeFromSuperview];
            self.titleController = nil;
            
            break;

        case kScreenPhoneNumberEntry:
            [self.phoneentryController.view removeFromSuperview];
            self.phoneentryController = nil;
            
            break;
        case kScreenPhoneNumberVerification:
            [self.phoneverificationController.view removeFromSuperview];
            self.phoneverificationController = nil;
            
            break;
        case kScreenMealsList:
            [self.mealslistController.view removeFromSuperview];

            
            break;
        case kScreenNewMeals:
            [self.newmealsController.view removeFromSuperview];
            
            
            break;
        default:
            break;
    }
}

-(void) addToAppView:(UIView*) view{
    AppDelegate* del = (AppDelegate*) [UIApplication sharedApplication].delegate;
    [del.rootviewController.view addSubview:view];
}
-(UIView*) getAppView{
    AppDelegate* del = (AppDelegate*) [UIApplication sharedApplication].delegate;
    return del.rootviewController.view;
}

+(id) sharedAppController{
    static AppController *sharedController = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedController = [[self alloc] init];
    });
    return sharedController;
}

-(id) init{
    self = [super init];
    if(self){
        
    }
    return self;
}
@end
