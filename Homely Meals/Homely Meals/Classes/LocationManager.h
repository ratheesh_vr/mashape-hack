//
//  LocationManager.h
//  iCrushiFlush
//
//  Created by Ratheesh V.R on 29/08/14.
//  Copyright (c) 2014 Outplay Labs. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@interface LocationManager : NSObject<CLLocationManagerDelegate>
@property (nonatomic, strong) CLLocationManager *locationManager;
@property (nonatomic) float latitude;
@property (nonatomic) float longitude;
-(void) startFindingLocation;
+(id) sharedLocationManager;
@end
