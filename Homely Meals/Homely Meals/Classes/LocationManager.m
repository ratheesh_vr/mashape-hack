//
//  LocationManager.m
//  iCrushiFlush
//
//  Created by Ratheesh V.R on 29/08/14.
//  Copyright (c) 2014 Outplay Labs. All rights reserved.
//

#import "LocationManager.h"
#import "AppState.h"

#define degreesToRadians(x) (M_PI * x / 180.0)
#define radiandsToDegrees(x) (x * 180.0 / M_PI)

@implementation LocationManager

-(void) startFindingLocation{
    [self.locationManager startUpdatingLocation];
}

- (float)getHeadingForDirectionFromCoordinate:(CLLocationCoordinate2D)fromLoc toCoordinate:(CLLocationCoordinate2D)toLoc
{
    float fLat = degreesToRadians(fromLoc.latitude);
    float fLng = degreesToRadians(fromLoc.longitude);
    float tLat = degreesToRadians(toLoc.latitude);
    float tLng = degreesToRadians(toLoc.longitude);
    
    float degree = radiandsToDegrees(atan2(sin(tLng-fLng)*cos(tLat), cos(fLat)*sin(tLat)-sin(fLat)*cos(tLat)*cos(tLng-fLng)));
    
    if (degree >= 0) {
        return degree;
    } else {
        return 360+degree;
    }
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation {
    
    CLLocationCoordinate2D currentCoordinates = newLocation.coordinate;
    
    self.latitude = currentCoordinates.latitude;
    self.longitude = currentCoordinates.longitude;
   // [[AppState sharedAppState] setLatitude:self.latitude];
   // [[AppState sharedAppState] setLongitude:self.longitude];
    
    
   // [self.locationManager stopUpdatingLocation];

}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    //NSLog(@"Unable to start location manager. Error:%@", [error description]);

}

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status{
    
    /* 
    if(status == kCLAuthorizationStatusDenied){
        //[self.buttonLogin setEnabled:YES];
        loginstate = LOGIN_STATE_NOT_CONNECTED;
        NSLog(@"NOt connected 2");
        [self setForNotConnected];
    }
    */
}


-(id) init{
    self = [super init];
    if(self){
        self.locationManager = [[CLLocationManager alloc] init];
        [self.locationManager setDelegate:self];
        //Only applies when in foreground otherwise it is very significant changes
        [self.locationManager setDesiredAccuracy:kCLLocationAccuracyBest];

        
        self.latitude = 19.017614700000000000;
        self.longitude = 72.856164400000010000;
        
    }
    return self;
}
+(id) sharedLocationManager{
    static LocationManager *sharedLocationManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedLocationManager = [[self alloc] init];
    });
    return sharedLocationManager;
}
@end
