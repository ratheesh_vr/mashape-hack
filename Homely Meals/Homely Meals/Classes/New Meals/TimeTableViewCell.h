//
//  TimeTableViewCell.h
//  Homely Meals
//
//  Created by Ratheesh V.R on 11/01/15.
//  Copyright (c) 2015 Outplay Labs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TimeTableViewCell : UITableViewCell
@property(nonatomic,weak) IBOutlet UILabel* labelTime;
@end
