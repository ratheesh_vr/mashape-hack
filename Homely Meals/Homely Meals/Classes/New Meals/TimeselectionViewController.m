//
//  TimeselectionViewController.m
//  Homely Meals
//
//  Created by Ratheesh V.R on 11/01/15.
//  Copyright (c) 2015 Outplay Labs. All rights reserved.
//

#import "TimeselectionViewController.h"
#import "TimeTableViewCell.h"

@interface TimeselectionViewController ()
{
    NSInteger selectedCellIndex;
}
@property(nonatomic,strong) NSMutableArray* arrayTime;
@end

@implementation TimeselectionViewController

-(IBAction)cancelSelected:(id)sender{
    [self dismissViewControllerAnimated:YES completion:Nil];
}
-(IBAction)doneSelected:(id)sender{
    if(selectedCellIndex != -1)
        self.parent.textfieldAvialabel.textColor = [self.arrayTime objectAtIndex:selectedCellIndex];
    [self dismissViewControllerAnimated:YES completion:Nil];
}
#pragma mark - UITableViewController


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
{
    
    return [self.arrayTime count];
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 50;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    static NSString *CellIdentifier = @"MealsCell";
    
    TimeTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil)
    {
        NSArray* views = [[NSBundle mainBundle] loadNibNamed:@"TimeTableViewCell" owner:nil options:nil];
        
        for (UIView *view in views) {
            if([view isKindOfClass:[UITableViewCell class]])
            {
                cell = (TimeTableViewCell*)view;
                
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                
            }
        }
    }
    
    cell.labelTime.text = [self.arrayTime objectAtIndex:indexPath.row];
    
    return cell;
}
-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    TimeTableViewCell *cell =  (TimeTableViewCell *) [tableView cellForRowAtIndexPath:indexPath];
    cell.labelTime.textColor = [UIColor redColor];
    if(selectedCellIndex != -1){
        NSIndexPath* ipath = [NSIndexPath indexPathForRow:selectedCellIndex inSection:0];
        TimeTableViewCell *cell1 =  (TimeTableViewCell *) [tableView cellForRowAtIndexPath:ipath];
        cell1.labelTime.textColor = [UIColor blackColor];
    }
    
    selectedCellIndex = indexPath.row;
}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    selectedCellIndex = -1;
    // Do any additional setup after loading the view from its nib.
    self.arrayTime = [[NSMutableArray alloc] initWithCapacity:100];
    
    [self.arrayTime addObject:@"6.00 AM"];
    [self.arrayTime addObject:@"7.00 AM"];
    [self.arrayTime addObject:@"8.00 AM"];
    [self.arrayTime addObject:@"9.00 AM"];
    
    [self.arrayTime addObject:@"10.00 AM"];
    [self.arrayTime addObject:@"11.00 AM"];
    [self.arrayTime addObject:@"12.00 PM"];
    [self.arrayTime addObject:@"1.00 PM"];
    
    [self.arrayTime addObject:@"2.00 PM"];
    [self.arrayTime addObject:@"3.00 PM"];
    [self.arrayTime addObject:@"4.00 PM"];
    [self.arrayTime addObject:@"5.00 PM"];
    
    [self.arrayTime addObject:@"6.00 PM"];
    [self.arrayTime addObject:@"7.00 PM"];
    [self.arrayTime addObject:@"8.00 PM"];
    [self.arrayTime addObject:@"9.00 PM"];
    

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
