//
//  TimeselectionViewController.h
//  Homely Meals
//
//  Created by Ratheesh V.R on 11/01/15.
//  Copyright (c) 2015 Outplay Labs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NewMealsViewController.h"

@interface TimeselectionViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>
@property(nonatomic,weak) IBOutlet UITableView* tableviewTime;
@property(nonatomic,weak) NewMealsViewController* parent;
-(IBAction)cancelSelected:(id)sender;
-(IBAction)doneSelected:(id)sender;

@end
