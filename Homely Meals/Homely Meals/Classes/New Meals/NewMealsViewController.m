//
//  NewMealsViewController.m
//  Homely Meals
//
//  Created by Ratheesh V.R on 11/01/15.
//  Copyright (c) 2015 Outplay Labs. All rights reserved.
//

#import "NewMealsViewController.h"
#import "AppController.h"
#import "Reachability.h"
#import "APIRequest.h"
#import "AppState.h"
#import "LocationManager.h"
#import "TimeselectionViewController.h"

@interface NewMealsViewController (){
    BOOL isShowingKeyBoard;
}
@property(nonatomic,strong) APIRequest* apirequest;
@property(nonatomic,strong) TimeselectionViewController* timeselectionController;
@end

@implementation NewMealsViewController

-(BOOL) textFieldShouldBeginEditing:(UITextField *)textField{
    if(!isShowingKeyBoard){
        self.scrollview.frame = CGRectMake(0, 66, 320, 280);
        isShowingKeyBoard = YES;
    }
    if(textField.tag == 100){
        if(!self.timeselectionController)
            self.timeselectionController = [[TimeselectionViewController alloc] initWithNibName:@"TimeselectionViewController" bundle:nil];
        self.timeselectionController.parent = self;
        [self presentViewController:self.timeselectionController animated:YES completion:Nil];
        return NO;
    }
    return YES;
}
-(BOOL) textViewShouldBeginEditing:(UITextView *)textView{
    if(!isShowingKeyBoard){
        self.scrollview.frame = CGRectMake(0, 66, 320, 280);
        isShowingKeyBoard = YES;
    }
    return YES;
}
-(IBAction)cancelSelected:(id)sender{
    self.scrollview.frame = CGRectMake(0, 66, 320, 502);
    [self.view endEditing:YES];
    [[AppController sharedAppController] showMealsList];
}
-(IBAction)doneSelected:(id)sender{
    self.scrollview.frame = CGRectMake(0, 66, 320, 502);
    [self.view endEditing:YES];
    if(![self getNetworkStatus]){
        UIAlertView* alertview = [[UIAlertView alloc] initWithTitle:@"No Internet" message:@"Please connect to internet" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alertview show];
        return;
    }
    
    
    NSString* tmpTime = [self.textfieldAvialabel.text stringByReplacingOccurrencesOfString:@" " withString:@""];
    if([tmpTime isEqualToString:@""]){
        UIAlertView* alertview = [[UIAlertView alloc] initWithTitle:@"Invalid Meal Time" message:@"Please enter proper Meal Time" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alertview show];
        return;
    }
    
    
    NSString* tmpMealsPrice = [self.textfieldPrice.text stringByReplacingOccurrencesOfString:@" " withString:@""];
    if([tmpMealsPrice isEqualToString:@""]){
        UIAlertView* alertview = [[UIAlertView alloc] initWithTitle:@"Invalid Meal Price" message:@"Please enter proper Meal Price" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alertview show];
        return;
    }
    
    
    NSString* tmpMealsQuantity = [self.textfieldMealsQntity.text stringByReplacingOccurrencesOfString:@" " withString:@""];
    if([tmpMealsQuantity isEqualToString:@""]){
        UIAlertView* alertview = [[UIAlertView alloc] initWithTitle:@"Invalid Meal Qantity" message:@"Please enter proper Meal Qantity" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alertview show];
        return;
    }
    
    
    NSString* tmpMealsInfo = [self.textfieldMealsInfo.text stringByReplacingOccurrencesOfString:@" " withString:@""];
    if([tmpMealsInfo isEqualToString:@""]){
        UIAlertView* alertview = [[UIAlertView alloc] initWithTitle:@"Invalid Meal Details" message:@"Please enter proper Meal Details" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alertview show];
        return;
    }
    
    
    NSString* tmpMealsName = [self.textfieldMealsName.text stringByReplacingOccurrencesOfString:@" " withString:@""];
    if([tmpMealsName isEqualToString:@""]){
        UIAlertView* alertview = [[UIAlertView alloc] initWithTitle:@"Invalid Meal Name" message:@"Please enter proper Meal Name" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alertview show];
        return;
    }
    
    [[AppController sharedAppController] showOverlayInView:self.view];
    /*
     "phone":"9539227770",
     "name":"biri",
     "quantity":"10",
     "price": "200",
     "available_upto":"ksjefjke"
     "latitude":"12.0",
     "longitude":"11.3"

    */
    

    
    AppState* appstate = [AppState sharedAppState];
    LocationManager* locationmanager = [LocationManager sharedLocationManager];
    
    NSString* latstr = [NSString stringWithFormat:@"%f",locationmanager.latitude];
    NSString* longstr = [NSString stringWithFormat:@"%f",locationmanager.longitude];
    NSDictionary* param = [[NSDictionary alloc] initWithObjectsAndKeys:
                           appstate.phonenumber, @"phone",
                           self.textfieldMealsName, @"name",
                           tmpMealsQuantity, @"quantity",
                           tmpMealsPrice, @"price",
                           self.textfieldAvialabel, @"available_upto",
                            latstr, @"latitude",
                            longstr, @"longitude",
                           
                           nil];
    
    //NSLog(@"PAR: %@",param);
    
    //NSString* urlstr = @"http://172.16.9.192:8000/foodentry/create/";
    NSString* urlstr = @"http://homelymeals.herokuapp.com/foodentry/create/";
    NSDictionary* data = [[NSDictionary alloc] initWithObjectsAndKeys:
                          urlstr,@"URL",
                          param,@"Param"
                          , nil];
    
    if(!self.apirequest)
        self.apirequest = [[APIRequest alloc] init];
    [self.apirequest postWithData:data ofType:1 delegate:self];
}


- (void) apiCallDidFinish:(NSData*) result header:(NSDictionary*) headerinfo ofType:(NSInteger) type{
    NSDictionary* data = (NSDictionary*)result;
    [[AppController sharedAppController] removeOverlay];
    NSInteger status = [[data valueForKey:@"status"] integerValue];
    if(status == 1){
         UIAlertView* alertview = [[UIAlertView alloc] initWithTitle:@"" message:@"Sucessfully added Meals" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alertview show];
    }
    else{
        UIAlertView* alertview = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Invalid verififcation code." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alertview show];
    }
}
- (void) apiCallDidFail:(NSString*) result ofType:(NSInteger) type{
    
    [[AppController sharedAppController] removeOverlay];
    UIAlertView* alertview = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Unable to connect. Please try after some time." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
    [alertview show];
    
}

-(BOOL) getNetworkStatus{
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    if( [reachability currentReachabilityStatus] == NotReachable)
        return NO;
    
    return YES;
}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    isShowingKeyBoard = NO;
    self.scrollview.contentSize = CGSizeMake(320, 502);
    self.mealAvialableTime = @"";
    self.apirequest = [[APIRequest alloc] init];
    [self setSingleTapGesture];
    // Do any additional setup after loading the view from its nib.
}

#pragma mark Single Tap
-(void) setSingleTapGesture{
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onSingleClickAction:)];
    [tapGesture setNumberOfTapsRequired:1];
    [tapGesture setNumberOfTouchesRequired:1];
    [self.view addGestureRecognizer:tapGesture];
}
- (void) onSingleClickAction:(UITapGestureRecognizer *) recognizer {
    if(isShowingKeyBoard){
         self.scrollview.frame = CGRectMake(0, 66, 320, 502);
        isShowingKeyBoard = NO;
    }
    [self.view endEditing:YES];
    
    
}
-(void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    self.scrollview.contentSize = CGSizeMake(320, 502);
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
