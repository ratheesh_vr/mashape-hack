//
//  NewMealsViewController.h
//  Homely Meals
//
//  Created by Ratheesh V.R on 11/01/15.
//  Copyright (c) 2015 Outplay Labs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewMealsViewController : UIViewController<UITextFieldDelegate,UITextViewDelegate>

@property(nonatomic,weak) IBOutlet UIScrollView* scrollview;
@property(nonatomic,strong) NSString* mealAvialableTime;
@property(nonatomic,weak) IBOutlet UITextField* textfieldMealsName ;
@property(nonatomic,weak) IBOutlet UITextField* textfieldMealsQntity ;
@property(nonatomic,weak) IBOutlet UITextField* textfieldPrice ;
@property(nonatomic,weak) IBOutlet UITextField* textfieldAvialabel ;
@property(nonatomic,weak) IBOutlet UITextView* textfieldMealsInfo ;
-(IBAction)cancelSelected:(id)sender;
-(IBAction)doneSelected:(id)sender;
@end
