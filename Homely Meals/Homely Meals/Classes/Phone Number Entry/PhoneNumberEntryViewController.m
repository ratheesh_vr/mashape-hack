//
//  PhoneNumberEntryViewController.m
//  Homely Meals
//
//  Created by Ratheesh V.R on 11/01/15.
//  Copyright (c) 2015 Outplay Labs. All rights reserved.
//

#import "PhoneNumberEntryViewController.h"
#import "AppController.h"
#import "Reachability.h"
#import "APIRequest.h"
#import "AppState.h"

@interface PhoneNumberEntryViewController ()
@property(nonatomic,strong) APIRequest* apirequest;
@end

@implementation PhoneNumberEntryViewController

-(IBAction)nextSelected:(id)sender{
    
    
    [self.view endEditing:YES];
    
    if(![self getNetworkStatus]){
        UIAlertView* alertview = [[UIAlertView alloc] initWithTitle:@"No Internet" message:@"Please connect to internet" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alertview show];
        return;
    }
    
    
   // [[AppController sharedAppController] showMealsList];
    //return;
    
    NSString* tmpCountryCode = [self.textfieldCountryCode.text stringByReplacingOccurrencesOfString:@" " withString:@""];
    if([tmpCountryCode isEqualToString:@""]){
        UIAlertView* alertview = [[UIAlertView alloc] initWithTitle:@"Invalid Country Code" message:@"Please enter proper country code" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alertview show];
        return;
    }
    
    NSString* tmpphonenumber = [self.textfieldPhoneNumer.text stringByReplacingOccurrencesOfString:@" " withString:@""];
    if([tmpphonenumber isEqualToString:@""]){
        UIAlertView* alertview = [[UIAlertView alloc] initWithTitle:@"Invalid Phone Number" message:@"Please enter proper phone number" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alertview show];
        return;
    }
    
   
    
    [[AppController sharedAppController] showOverlayInView:self.view];
    
    NSString* phonenum = [NSString stringWithFormat:@"%@%@",tmpCountryCode,tmpphonenumber];
    
    [[AppState sharedAppState] updatePhoneNumber:phonenum];
    
    NSDictionary* param = [[NSDictionary alloc] initWithObjectsAndKeys:
                           phonenum, @"phone",

                           nil];
    //NSString* urlstr = @"http://172.16.9.192:8000/homelymealsuser/create/";
    NSString* urlstr = @"http://homelymeals.herokuapp.com/homelymealsuser/create/";
    NSDictionary* data = [[NSDictionary alloc] initWithObjectsAndKeys:
                          urlstr,@"URL",
                          param,@"Param"
                          , nil];
    
    if(!self.apirequest)
        self.apirequest = [[APIRequest alloc] init];
    [self.apirequest postWithData:data ofType:1 delegate:self];
    //[[AppController sharedAppController] showPhoneVerification];
}

- (void) apiCallDidFinish:(NSData*) result header:(NSDictionary*) headerinfo ofType:(NSInteger) type{
    [[AppController sharedAppController] removeOverlay];
    NSDictionary* data = (NSDictionary*)result;
    [[AppController sharedAppController] removeOverlay];
    NSInteger status = [[data valueForKey:@"status"] integerValue];
    if(status == 1){
        [[AppController sharedAppController] showPhoneVerification];
    }
    else{
        UIAlertView* alertview = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Unable to connect. Please try after some time." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alertview show];
    }
    NSLog(@"LOgin Data: %@",data);
}
- (void) apiCallDidFail:(NSString*) result ofType:(NSInteger) type{
    NSLog(@"LOgin fail: %@",result);
    [[AppController sharedAppController] removeOverlay];
    UIAlertView* alertview = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Unable to connect. Please try after some time." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
    [alertview show];
    
}

-(BOOL) getNetworkStatus{
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    if( [reachability currentReachabilityStatus] == NotReachable)
        return NO;
    
    return YES;
}


#pragma mark Single Tap
-(void) setSingleTapGesture{
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onSingleClickAction:)];
    [tapGesture setNumberOfTapsRequired:1];
    [tapGesture setNumberOfTouchesRequired:1];
    [self.view addGestureRecognizer:tapGesture];
}
- (void) onSingleClickAction:(UITapGestureRecognizer *) recognizer {
    [self.view endEditing:YES];
    
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self setSingleTapGesture];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
